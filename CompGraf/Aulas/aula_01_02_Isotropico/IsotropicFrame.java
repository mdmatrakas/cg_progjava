package aula_01_02_Isotropico;

// O modo de mapeamento isotr�pico
// Origem do sistema de coordenadas l�gicas no centro da �rea de desenho
// Eixo y positivo para cima. Quadrado (girado 45 graus) cabe na �rea de 
// desenho. O click do mouse exibe as coodenadas l�gicas do ponto selecionado.

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class IsotropicFrame extends JFrame
{
	IsotropicFrame()
	{
		super("Modo de mapeamento isotr�pico.");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(400, 300);
		add("Center", new IsotropicPanel());
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setVisible(true);
	}
}
