package cap03;

import baseCG.Point3D;

// Rotacao3DTest: Rota��o de um cubo em torno de um eixo 
// paralelo a uma diagonal do seu plano superior
// Usa: Point3D, Rotacao3D

public class Rotacao3DTest
{
	public static void main(String[] args)
	{
		Point3D a = new Point3D(0, 0, 2);
		Point3D b = new Point3D(1, 1, 2);
		double alpha = Math.PI;
		
		// Especifica AB como eixo orientado de rota��o e alpha como �ngulo de rota��o
		Rotacao3D.initRotate(a, b, alpha);
		
		// Vertices de um cubo: 0, 1, 2, 3 ,4 na parte inferior,
		// 4, 5, 6, 7 na parte superior. Vertice 0 na origem
		Point3D[] v = {
				new Point3D(0, 0, 0),
				new Point3D(1, 0, 0),
				new Point3D(1, 1, 0),
				new Point3D(0, 1, 0),
				new Point3D(0, 0, 1),
				new Point3D(1, 0, 1),
				new Point3D(1, 1, 1),
				new Point3D(0, 1, 1)
		};
		
		System.out.println("Cubo rotacionado em 180 graus em torne de AB,");
		System.out.println("em que A = (" + a.getX() + ", " + a.getY() + ", " + a.getZ() + ") e B = (" + b.getX() + ", " + b.getY() + ", " + b.getZ() + ");");
		System.out.println("Vertices do cubo: ");
		System.out.println("    Antes da rota��o    Ap�s da rota��o");
		for(int i = 0; i < 8; i++)
		{
			Point3D p = v[i];
			// Calcula P1, o resultado da rota��o de P:
			Point3D p1 = Rotacao3D.rotate(p);
			System.out.println(i + ":  " + p.getX() + " " + p.getY() + " " + p.getZ() + "         " + 
					f(p1.getX()) + " " + f(p1.getY()) + " " + f(p1.getZ()));
		}
	}
	
	static double f(double x)
	{
		return Math.abs(x) < 1e-10 ? 0.0 : x;
	}
}
