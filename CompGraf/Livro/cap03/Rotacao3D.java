package cap03;

import baseCG.Point3D;

// Rotacao3D.java: Classe usado por outros programas para rota��es em torno de um eixo arbitrario
// Usa: Point3D

public class Rotacao3D
{
	static double r11, r12, r13, r21, r22, r23, r31, r32, r33, r41, r42, r43;
	
	/* O m�todo initRotate calcula a matriz geral de rota�ao
	 * 	 *  
	 *       | r11 r12 r13 0 |
	 *   R = | r21 r22 r23 0 |
	 *       | r31 r32 r33 0 |
	 *       | r41 r42 r43 1 |
	 * 
	 *       a ser usada como [x1 y1 z1 1] = [x y z 1] * R pelo metodo 'rotate'
	 *       O ponto (x1, y1, z1) � a imagem de (x, y, z).
	 *       A rota��o ocorre em torno do eixo orientado AB e por um angulo alfa.
	 */
	static void initRotate(Point3D a, Point3D b, double alfa)
	{
		double v1 = b.getX() - a.getX();
		double v2 = b.getY() - a.getY();
		double v3 = b.getZ() - a.getZ();
		double theta = Math.atan2(v2, v1);
		double phi = Math.atan2(Math.sqrt(v1*v1 + v2*v2), v3);
		initRotate(a, theta, phi, alfa);
	}
	
	static void initRotate(Point3D a, double theta, double phi, double alfa)
	{
		double cosAlfa, sinAlfa, cosPhi, sinPhi, cosTheta, sinTheta, cosPhi2, sinPhi2, cosTheta2, sinTheta2, 
		 c, a1 = a.getX(), a2 = a.getY(), a3 = a.getZ(); //pi,
		
		cosPhi = Math.cos(phi);
		sinPhi = Math.sin(phi);
		cosPhi2 = cosPhi * cosPhi;
		sinPhi2 = sinPhi * sinPhi;
		
		cosTheta = Math.cos(theta);
		sinTheta = Math.sin(theta);
		cosTheta2 = cosTheta * cosTheta;
		sinTheta2 = sinTheta * sinTheta;

		cosAlfa = Math.cos(alfa);
		sinAlfa = Math.sin(alfa);

		c = 1.0 - cosAlfa;
		
		r11 = cosTheta2 * (cosAlfa * cosPhi2 + sinPhi2) + cosAlfa * sinTheta2;
		r12 = sinAlfa * cosPhi + c * sinPhi2 * cosTheta * sinTheta;
		r13 = sinPhi * (cosPhi * cosTheta * c - sinAlfa * sinTheta);
		
		r21 = sinPhi2 * cosTheta * sinTheta * c - sinAlfa * cosPhi;
		r22 = sinTheta2 * (cosAlfa * cosPhi2 + sinPhi2) + cosAlfa * cosTheta2;
		r23 = sinPhi * (cosPhi * sinTheta * c + sinAlfa * cosTheta); 
		
		r31 = sinPhi * (cosPhi * cosTheta * c + sinAlfa * sinTheta); 
		r32 = sinPhi * (cosPhi * sinTheta * c - sinAlfa * cosTheta);
		r33 = cosAlfa * sinPhi2 + cosPhi2;
		
		r41 = a1 - a1 * r11 - a2 * r21 - a3 * r31;
		r42 = a2 - a1 * r12 - a2 * r22 - a3 * r32;
		r43 = a3 - a1 * r13 - a2 * r23 - a3 * r33;
	}
	
	static Point3D rotate(Point3D p)
	{
		return new Point3D(
				(float)(p.getX() * r11 + p.getY() * r21 + p.getZ() * r31 + r41),
				(float)(p.getX() * r12 + p.getY() * r22 + p.getZ() * r32 + r42),
				(float)(p.getX() * r13 + p.getY() * r23 + p.getZ() * r33 + r43));
		
	}
}
