package baseCG;

import java.awt.Graphics;


// Tools2D.java: Class to be used in other program files.
// Uses: Point2D and Triangle.

/* CGDemo is a companion of the textbook

 L. Ammeraal and K. Zhang, Computer Graphics for Java Programmers, 
 2nd Edition, Wiley, 2006.

 Copyright (C) 2006  Janis Schubert, Kang Zhang, Leen Ammeraal 

 This program is free software; you can redistribute it and/or 
 modify it under the terms of the GNU General Public License as 
 published by the Free Software Foundation; either version 2 of 
 the License, or (at your option) any later version. 

 This program is distributed in the hope that it will be useful, 
 but WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 See the GNU General Public License for more details.  

 You should have received a copy of the GNU General Public 
 License along with this program; if not, write to 
 the Free Software Foundation, Inc., 51 Franklin Street, 
 Fifth Floor, Boston, MA  02110-1301, USA. 
 */

public class Tools2D
{
	public static float area2(Point2D A, Point2D B, Point2D C)
	{
		return (A.getX() - C.getX()) * (B.getY() - C.getY()) - (A.getY() - C.getY()) * (B.getX() - C.getX());
	}
	
	public static float area(Point2D A, Point2D B, Point2D C)
	{
		return Math.abs(area2(A, B, C) / 2);
	}
	
	public static float area2(Point2D[] pol)
	{
		int n = pol.length;
		int j = n - 1;
		float a = 0;
		
		for(int i = 0; i < n; i++)
		{
			// i e j denotam vertices vizinhos com i um passo a frente de j
			a += pol[j].getX() * pol[i].getY() - pol[j].getY() * pol[i].getX();
			j = i;
		}
		return a;
	}

	public static float area(Point2D[] pol)
	{
		return Math.abs(area2(pol) / 2);
	}
	// ABC is assumed to be counter-clockwise
	// Assume-se que ABC � anti-hor�rio
	public static boolean insideTriangle(Point2D A, Point2D B, Point2D C, Point2D P)
	{
		return Tools2D.area2(A, B, P) >= 0 && Tools2D.area2(B, C, P) >= 0
				&& Tools2D.area2(C, A, P) >= 0;
	}

	public static boolean insidePoligon(Point2D p, Point2D[] pol)
	{
		int n = pol.length;
		int j = n - 1;

		boolean b = false;

		for (int i = 0; i < n; i++)
		{
			if (pol[i].getX() >= p.getX() || pol[j].getX() >= p.getX())
				if (pol[j].getY() <= p.getY() && p.getY() < pol[i].getY()
						&& area2(pol[j], pol[i], p) > 0 || pol[i].getY() <= p.getY()
						&& p.getY() < pol[j].getY() && area2(pol[i], pol[j], p) > 0)
					b = !b;
			j = i;
		}
		return b;
	}

	public static boolean onSegment(Point2D a, Point2D b, Point2D p)
	{
		double dx = b.getX() - a.getX();
		double dy = b.getY() - a.getY();
		double eps = 0.001 * (dx * dx + dy * dy);

		return (a.getX() != b.getX()
				&& (a.getX() <= p.getX() && p.getX() <= b.getX() || b.getX() <= p.getX() && p.getX() <= a.getX()) || a.getX() == b.getX()
				&& (a.getY() <= p.getY() && p.getY() <= b.getY() || b.getY() <= p.getY() && p.getY() <= a.getY()))
				&& Math.abs(area2(a, b, p)) < eps;
	}

	// P' (P projetado sobre AB) pertence ao segmento fechado AB
	public static boolean projOnClosedSegment(Point2D a, Point2D b, Point2D p)
	{
		double dx = b.getX() - a.getX();
		double dy = b.getY() - a.getY();
		double eps = 0.001 * (dx * dx + dy * dy);
		double len2 = dx * dx + dy * dy;
		double inprod = dx * (p.getX() - a.getX()) + dy * (p.getY() - a.getY());
		return inprod > -eps && inprod < len2 + eps;
	}
	
	// P' (P projetado sobre AB) pertence ao segmento aberto AB
	public static boolean projOnOpenSegment(Point2D a, Point2D b, Point2D p)
	{
		double dx = b.getX() - a.getX();
		double dy = b.getY() - a.getY();
		double eps = 0.001 * (dx * dx + dy * dy);
		double len2 = dx * dx + dy * dy;
		double inprod = dx * (p.getX() - a.getX()) + dy * (p.getY() - a.getY());
		return inprod > eps && inprod < len2 - eps;
	}
	
	// Calcule P' (P projetado sobre AB)
	public static Point2D projection(Point2D a, Point2D b, Point2D p)
	{
		float vx = b.getX() - a.getX();
		float vy = b.getY() - a.getY();
		float len2 = vx * vx + vy * vy;
		float inprod = vx * (p.getX() - a.getX()) + vy * (p.getY() - a.getY());
		
		return new Point2D(a.getX() + inprod * vx / len2, a.getY() + inprod * vy / len2);
	}

	// Calcule P', a proje��o de P sobre a reta r definida por
	// ax + by = h em que a * a + b * b = 1
	public static Point2D projection(float a, float b, float h, Point2D p)
	{
		float d = p.getX() * a + p.getY() * b - h;
		return new Point2D(p.getX() - d * a, p.getY() - d * b);
	}
	
	// Calcule o quadrado da distancia entre P e o segmento AB
	public static float distance2(Point2D a, Point2D b, Point2D p)
	{
		return distance2(p, projection(a, b, p));
	}
	
	// Calcule o quadrado da distancia entre P e a reta r definida por
	// ax + by = h em que a * a + b * b = 1
	public static float distance2(float a, float b, float h, Point2D p)
	{
		return distance2(p, projection(a, b, h, p));
	}
	
	// Calcule a distancia entre P e o segmento AB
	public static float distance(Point2D a, Point2D b, Point2D p)
	{
		return distance(p, projection(a, b, p));
	}
	
	// Calcule a distancia entre P e a reta r definida por
	// ax + by = h em que a * a + b * b = 1
	public static float distance(float a, float b, float h, Point2D p)
	{
		return distance(p, projection(a, b, h, p));
	}

	public static float distance2(Point2D P, Point2D Q)
	{
		float dx = P.getX() - Q.getX(), dy = P.getY() - Q.getY();
		return dx * dx + dy * dy;
	}

	public static float distance(Point2D P, Point2D Q)
	{
		float dx = P.getX() - Q.getX(), dy = P.getY() - Q.getY();
		return (float)Math.sqrt(dx * dx + dy * dy);
	}

	public static void triangulate(Point2D[] P, Triangle[] tr)
	{ // P contains all n polygon vertices in CCW order.
		// The resulting triangles will be stored in array tr.
		// This array tr must have length n - 2.
		int n = P.length, j = n - 1, iA = 0, iB, iC;
		int[] next = new int[n];
		for (int i = 0; i < n; i++)
		{
			next[j] = i;
			j = i;
		}
		for (int k = 0; k < n - 2; k++)
		{ // Find a suitable triangle, consisting of two edges
			// and an internal diagonal:
			Point2D A, B, C;
			boolean triaFound = false;
			int count = 0;
			while (!triaFound && ++count < n)
			{
				iB = next[iA];
				iC = next[iB];
				A = P[iA];
				B = P[iB];
				C = P[iC];
				if (Tools2D.area2(A, B, C) >= 0)
				{ // Edges AB and BC; diagonal AC.
					// Test to see if no other polygon vertex
					// lies within triangle ABC:
					j = next[iC];
					while (j != iA && !insideTriangle(A, B, C, P[j]))
						j = next[j];
					if (j == iA)
					{ // Triangle ABC contains no other vertex:
						tr[k] = new Triangle(A, B, C);
						next[iA] = iC;
						triaFound = true;
					}
				}
				iA = next[iA];
			}
			if (count == n)
			{
				System.out.println("Not a simple polygon"
						+ " or vertex sequence not counter-clockwise.");
				System.exit(1);
			}
		}
	}

	public static boolean ccw(Point2D[] p)
	{
		int n = p.length;
		int k = 0;

		for (int i = 1; i < n; i++)
		{
			if (p[i].getX() <= p[k].getX() && (p[i].getX() < p[k].getX() || p[i].getY() < p[k].getY()))
				k = i;
		}
		// p[k] � um vertice convexo.
		int prev = k - 1;
		int next = k + 1;
		if (prev == -1)
			prev = n - 1;
		if (next == n)
			next = 0;
		return area2(p[prev], p[k], p[next]) > 0;
	}
	
	public static void putPixel(Graphics g, int x, int y)
	{
		g.drawLine(x, y, x, y);
	}
	
	public static void drawLine1(Graphics g, int xP, int yP, int xQ, int yQ)
	{
		int x = xP;
		int y = yP;
		float d = 0F;
		float m = (float)(yQ - yP) / (float)(xQ - xP);
		
		for(;;)
		{
			putPixel(g, x, y);
			if(x == xQ)
				break;
			x++;
			d += m;
			if(d >= 0.5)
			{
				y++;
				d--;
			}
		}
	}
	
	public static void drawLine2(Graphics g, int xP, int yP, int xQ, int yQ)
	{
		int x = xP;
		int y = yP;
		int d = 0;
		int dx = xQ - xP;
		int c = 2 * dx;
		int m = 2 * (yQ - yP);
		
		for(;;)
		{
			putPixel(g, x, y);
			if(x == xQ)
				break;
			x++;
			d += m;
			if(d >= dx)
			{
				y++;
				d -= c;
			}
		}
	}
	
	public static void drawLine(Graphics g, int xP, int yP, int xQ, int yQ)
	{
		int x = xP;
		int y = yP;
		int d = 0;
		int dx = xQ - xP;
		int dy = yQ - yP;
		int xInc = 1;
		int yInc = 1;
		int c;
		int m;
		
		if(dx < 0)
		{ 
			xInc = -1;
			dx = -dx;
		}
		if(dy < 0)
		{
			yInc = -1;
			dy = -dy;
		}
		if(dy <= dx)
		{
			c = 2 * dx;
			m = 2 * dy;
			if(xInc < 0) 
				dx++;
			for(;;)
			{
				putPixel(g, x, y);
				if(x == xQ)
					break;
				x += xInc;
				d += m;
				if(d >= dx)
				{
					y += yInc;
					d -= c;
				}				
			}
		}
		else
		{
			c = 2 * dy;
			m = 2 * dx;
			if(yInc < 0) 
				dy++;
			for(;;)
			{
				putPixel(g, x, y);
				if(y == yQ)
					break;
				y += yInc;
				d += m;
				if(d >= dy)
				{
					x += xInc;
					d -= c;
				}
			}
		}
	}
	
	public static void doubleStep1(Graphics g, int xP, int yP, int xQ, int yQ)
	{
		int dx, dy, x, y, yInc;
		
		if(xP >= xQ)
		{
			if(xP == xQ) // n�o permitido porque dividimos por dx (= xQ - xP)
				return; 
			// xP > xQ, ent�o permute os pontos P e Q
			int t;
			t = xP;
			xP = xQ;
			xQ = t;
			t = yP;
			yP = yQ;
			yQ = t;
		}
		// Agora xP < xQ
		if(yQ >= yP) // Caso normal, yP < yQ
		{
			yInc = 1;
			dy = yQ - yP;
		}
		else
		{
			yInc = -1;
			dy = yP - yQ;
		}
		dx = xQ - xP; // dx > 0, dy > 0
		
		float d = 0F; // erro d = yExato - y
		float m = (float)dy / (float)dx; // m <= 1, m = | inclina��o |
		
		putPixel(g, xP, yP);
		y = yP;
		
		for(x = xP; x < xQ - 1; )
		{
			if(d + 2 * m < 0.5) // padr�o 1
			{
				putPixel(g, ++x, y);
				putPixel(g, ++x, y);
				d += 2 * m; // O erro aumenta em 2m, ja que y permanece innalterado e yExato aumenta em 2m
			}
			else
			{
				if(d + 2 * m < 1.5) // padr�o 2 ou 3
				{
					if(d + m < 0.5) // padr�o 2
					{
						putPixel(g, ++x, y);
						putPixel(g, ++x, y += yInc);
						d += 2 * m - 1; // Devido a ++y, o erro � agora 1 a menos que com o padr�o 1
					}
					else // padr�o 3
					{
						putPixel(g, ++x, y += yInc);
						putPixel(g, ++x, y);
						d += 2 * m - 1; // Mesmo do padr�o 2
					}
				}
				else // padr�o 4
				{
					putPixel(g, ++x, y += yInc);
					putPixel(g, ++x, y += yInc);
					d += 2 * m - 2; // Devido a y+=2, o erro � agora 2 a menos que com o padr�o 1
				}
			}
		}
		if(x < xQ) // x = xQ - 1
		{
			putPixel(g, xQ, yQ);
		}
	}

	public static void doubleStep2(Graphics g, int xP, int yP, int xQ, int yQ)
	{
		int dx, dy, x, y, yInc;
		
		if(xP >= xQ)
		{
			if(xP == xQ) // n�o permitido porque dividimos por dx (= xQ - xP)
				return; 
			// xP > xQ, ent�o permute os pontos P e Q
			int t;
			t = xP;
			xP = xQ;
			xQ = t;
			t = yP;
			yP = yQ;
			yQ = t;
		}
		// Agora xP < xQ
		if(yQ >= yP) // Caso normal, yP < yQ
		{
			yInc = 1;
			dy = yQ - yP;
		}
		else
		{
			yInc = -1;
			dy = yP - yQ;
		}
		dx = xQ - xP; // dx > 0, dy > 0
		
		int dy4 = dy * 4;
		int v = dy4 - dx;
		int dx2 = 2 * dx;
		int dy2 = 2 * dy;
		int dy4MenosDx2 = dy4 - dx2;
		int dy4MenosDx4 = dy4MenosDx2 - dx2;
		
		putPixel(g, xP, yP);
		y = yP;
		
		for(x = xP; x < xQ - 1; )
		{
			if(v < 0) // padr�o 1
			{
				putPixel(g, ++x, y);
				putPixel(g, ++x, y);
				v += dy4;
			}
			else
			{
				if(v < dx2) // padr�o 2 ou 3
				{
					if(v < dy2) // padr�o 2
					{
						putPixel(g, ++x, y);
						putPixel(g, ++x, y += yInc);
						v += dy4MenosDx2;
					}
					else // padr�o 3
					{
						putPixel(g, ++x, y += yInc);
						putPixel(g, ++x, y);
						v += dy4MenosDx2;
					}
				}
				else // padr�o 4
				{
					putPixel(g, ++x, y += yInc);
					putPixel(g, ++x, y += yInc);
					v += dy4MenosDx4;
				}
			}
		}
		if(x < xQ) 
		{
			putPixel(g, xQ, yQ);
		}
	}
	
	public static void drawCircle(Graphics g, int xC, int yC, int r)
	{
		int x = 0;
		int y = r;
		int u = 1;
		int v = 2 * r - 1;
		int e = 0;
		while(x < y)
		{
			putPixel(g, xC + x, yC + y);
			putPixel(g, xC + y, yC - x);
			putPixel(g, xC - x, yC - y);
			putPixel(g, xC - y, yC + x);

			x++;
			e += u;
			u+= 2;
			if(v < 2 * e)
			{
				y--;
				e -= v;
				v -= 2;
			}
			if(x > y)
				break;
			
			putPixel(g, xC + y, yC + x);
			putPixel(g, xC + x, yC - y);
			putPixel(g, xC - y, yC - x);
			putPixel(g, xC - x, yC + y);
		}
	}
}
